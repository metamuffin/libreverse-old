
export type Packet = PacketCS | PacketSC
export type PacketSC = PWelcome | PUpdateEntity | PRemoveEntity | PAnimateEntity
export type PacketCS = PWelcome | PInputWalk | PSpawnPlayer | PInputInteract
export type BézierCurve = "linear" | "quadratic" | "cubic" | "HYPERCUBIC"

export interface Vec3 { x: number, y: number, z: number }
export type IPosition = Vec3
export type IRotation = Vec3
export type IScale = Vec3

export interface PWelcome {
    kind: "welcome",
    time: number,
    supported_extensions: string[]
    required_extensions: string[]
}

export interface PUpdateEntity extends IPartialEntity {
    kind: "update_entity"
}

export interface PAnimateEntity {
    kind: "animate_entity"
    type: BézierCurve
    points: { time: number, value: IPartialEntity }[]
}

export interface PRemoveEntity {
    kind: "remove_entity"
    id: string
}

export interface PSpawnPlayer {
    kind: "spawn_player"
    name: string
}

export interface PInputWalk {
    kind: "input", action: "walk",
    target: IPosition
}
export interface PInputInteract {
    kind: "input", action: "interact",
    object: string
}

export interface IPartialEntity {
    id: string
    type?: "camera" | "model" | "light"
    
    position?: IPosition,
    rotation?: IRotation,
    scale?: IScale,

    clickable?: boolean

    path?: string
    intensity?: number
    color?: number
    fov?: number
}
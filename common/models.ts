import { IPartialEntity, IPosition, IRotation, IScale } from "./packet"

export abstract class MEntity {

    id: string = ""
    position: IPosition = { x: 0, y: 0, z: 0 }
    rotation: IRotation = { x: 0, y: 0, z: 0 }
    scale: IScale = { x: 0, y: 0, z: 0 }

    constructor(p: IPartialEntity) {
        this.apply_update(p)
    }

    static spawn(p: IPartialEntity) {
        if (p.type == "camera") return new MCamera(p)
        if (p.type == "light") return new MLight(p)
        if (p.type == "model") return new MModel(p)
    }
    
    apply_update(p: IPartialEntity) {
        if (p.position !== undefined) this.position = p.position
        if (p.scale !== undefined) this.scale = p.scale
        if (p.rotation !== undefined) this.rotation = p.rotation
    }
}

export class MModel extends MEntity {
    path: string = ""
    apply_update(p: IPartialEntity) {
        super.apply_update(p)
        if (p.path !== undefined) this.path = p.path
    }
}
export class MLight extends MEntity {
    intensity: number = 0
    color: number = 0
    apply_update(p: IPartialEntity) {
        super.apply_update(p)
        if (p.color !== undefined) this.color = p.color
        if (p.intensity !== undefined) this.intensity = p.intensity
    }
}
export class MCamera extends MEntity {
    fov: number = 0
    apply_update(p: IPartialEntity) {
        super.apply_update(p)
        if (p.fov !== undefined) this.fov = p.fov
    }
}

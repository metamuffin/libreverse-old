import { BézierCurve, IPartialEntity, PAnimateEntity } from "./packet"
import { IDontKnowWhatToDoHereError, PleaseImplementThisError, YourMomIsNotImplementedError } from "./utils"

export interface PatchableEntity { id: string, apply_update: (p: IPartialEntity) => void }

export abstract class Animation {
    static queued: Animation[] = []
    static running: Animation[] = []

    static queue_animation(a: Animation) {
        // todo replace with insertion sort
        this.queued.push(a)
        this.queued.sort((a, b) => a.start - b.start)
    }

    static check_queue(time: number) {
        while (this.queued[0]) {
            if (this.queued[0].start < time) {
                const a = this.queued.shift()
                if (!a) throw new Error("impossible");
                this.running.push(a)
            } else break
        }
    }

    static update_running(time: number) {
        this.running = this.running.filter(a => a.end >= time)
        this.running.forEach(a => a.update(time))
    }

    static update(time: number) {
        this.check_queue(time)
        this.update_running(time)
    }

    start: number
    end: number
    points: { time: number, value: IPartialEntity }[]
    type: BézierCurve

    abstract target: PatchableEntity

    constructor(p: PAnimateEntity) {
        this.start = p.points[0].time
        this.end = p.points[p.points.length - 1].time
        this.points = p.points
        this.type = p.type
        if ("no" == this.points.reduce((a, v) => a == v.value.id ? v.value.id : "no!", this.points[0].value.id)) throw new Error("no!");
    }

    update(t: number) {
        const u = this.get_interpolated(t)
        this.target.apply_update(u)
    }

    get_interpolated(t: number): IPartialEntity {
        if (this.type == "linear") {
            const index = this.points.findIndex(v => v.time >= t) - 1
            const from = this.points[index]
            const to = this.points[index + 1]
            if (!to || !from) throw new IDontKnowWhatToDoHereError();

            let u: IPartialEntity = { id: this.target.id }
            const inf = (a: number, b: number) => bez_linear_timed(from.time, to.time, a, b, t)

            if (from.value.position && to.value.position) {
                u.position = {
                    x: inf(from.value.position.x, to.value.position.x),
                    y: inf(from.value.position.y, to.value.position.y),
                    z: inf(from.value.position.z, to.value.position.z),
                }
            }
            return u
        } else throw new YourMomIsNotImplementedError();
    }
}

function bez_linear_timed(ta: number, tb: number, xa: number, xb: number, t: number) {
    return bez_linear(xa, xb, (t - ta) / (tb - ta))
}
function bez_linear(xa: number, xb: number, v: number) {
    return xa * (1 - v) + xb * v
}

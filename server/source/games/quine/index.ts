import { IPosition } from "../../../../common/packet";
import { Client } from "../../lib/client";
import { Game } from "../../lib/game";

class Nene extends Game {
    cube_position: IPosition = { x: 0, y: 0, z: 0 }
    clients: Set<Client> = new Set()

    constructor() {
        super()
    }

    on_client(c: Client) {
        console.log(`client connected`);
        this.clients.add(c)
        c.send_packet({ kind: "update_entity", id: "a", type: "model", clickable: false, position: this.cube_position, rotation: { x: 0, y: 0, z: 0 }, path: "/assets/debug.glb" })
        c.send_packet({ kind: "update_entity", id: "floor", type: "model", position: { x: 0, y: -2, z: 0 }, scale: { x: 10, y: 1, z: 10 }, path: "/assets/debug.glb" })
        c.send_packet({ kind: "update_entity", id: "b", type: "light", position: { x: 5, y: 5, z: 5 }, rotation: { x: 0, y: 0, z: 0 }, color: 0xFFFFFF, intensity: 1 })
        c.send_packet({ kind: "update_entity", id: "c", type: "camera", position: { x: 0, y: 0, z: 10 }, rotation: { x: 0, y: 0, z: 0 } })
        c.on_packet = p => {
            if (p.kind == "input") {
                if (p.action == "walk") {
                    this.clients.forEach(c => {
                        c.send_packet({
                            kind: "animate_entity",
                            type: "linear",
                            points: [
                                { time: Date.now(), value: { id: "a", position: this.cube_position } },
                                { time: Date.now() + 1000, value: { id: "a", position: p.target } },
                            ],
                        })
                    })
                    this.cube_position = p.target
                }
            }
        }
        c.on_disconnect = () => {
            console.log("client disconnected");
            this.clients.delete(c)
        }
    }
}

new Nene().run()

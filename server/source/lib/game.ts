
import express from "express"
import expressWs from "express-ws"
import { Packet, PacketSC, PacketCS } from "../../../common/packet"
import { Client, time } from "./client"

export abstract class Game {
    constructor() {

    }

    run() {
        const app_e = express()
        const app = expressWs(app_e).app

        app.ws("/", (ws, req) => {
            const c = new Client(ws)
            this.on_client(c)
        })

        const PORT = parseInt(process.env.PORT ?? "8081")
        const HOST = process.env.HOST ?? "0.0.0.0"
        app.listen(PORT, HOST, () => console.log(`listening on ${HOST}:${PORT}`))

    }

    abstract on_client(c: Client): void
}


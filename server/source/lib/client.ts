import { PacketCS, PacketSC } from "../../../common/packet";


export function time() {
    return Date.now()
}

function ws_send(ws: any, data: PacketSC) {
    try { ws.send(JSON.stringify(data)) } catch (e) { console.log("express-ws sucks"); }
}

export class Client {
    on_packet: (p: PacketCS) => void = () => { }
    on_disconnect: () => void = () => { }

    constructor(protected ws: any) {
        this.send_packet({ kind: "welcome", time: time(), required_extensions: [], supported_extensions: [] })
        ws.onclose = () => {
            this.on_disconnect()
        }
        ws.onmessage = (m: any) => {
            let j;
            try { j = JSON.parse(m.data.toString()) } catch (e) { }
            if (j) {
                //@ts-ignore
                this.on_packet(j)
            }
        }
    }

    send_packet(p: PacketSC) { ws_send(this.ws, p) }
}

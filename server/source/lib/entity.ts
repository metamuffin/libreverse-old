import { PatchableEntity } from "../../../common/animation";
import { IPartialEntity, IPosition, IRotation, IScale } from "../../../common/packet";
import { Vector } from "./vector";

export interface EntityContainer {
    schedule_broadcast(p: IPartialEntity): void
}

export class Entity implements PatchableEntity {
    public id: string = Math.floor(Math.random() * 0xFFFFFF).toString(16)

    public position: Vector = new Vector({ x: 0, y: 0, z: 0 });
    public scale: Vector = new Vector({ x: 0, y: 0, z: 0 });
    public rotation: Vector = new Vector({ x: 0, y: 0, z: 0 });

    public container?: EntityContainer

    constructor() {
        this.position.add_listener(() => this.property_updated({ id: this.id, position: this.position.export() }))
        this.rotation.add_listener(() => this.property_updated({ id: this.id, rotation: this.rotation.export() }))
        this.scale.add_listener(() => this.property_updated({ id: this.id, scale: this.scale.export() }))
    }

    apply_update(p: IPartialEntity) {
        if (p.position !== undefined) this.position.set(p.position.x, p.position.y, p.position.z)
        if (p.scale !== undefined) this.object.scale.set(p.scale.x, p.scale.y, p.scale.z)
        if (p.rotation !== undefined) this.object.rotation.set(p.rotation.x, p.rotation.y, p.rotation.z)
        if (p.clickable !== undefined) this.clickable = p.clickable
    }

    property_updated(p: IPartialEntity) {
        this.container?.schedule_broadcast(p)
    }

    export() {

    }

}

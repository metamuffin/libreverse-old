import { Vec3 } from "../../../common/packet"


export class Vector {
    private _x = 0
    private _y = 0
    private _z = 0


    constructor(p: Vec3) {
        this.x = p.x
        this.y = p.y
        this.z = p.z
    }

    public export(): Vec3 {
        return { x: this._x, y: this._y, z: this._z }
    }

    public get x() {
        return this._x
    }
    public set x(value) {
        this._x = value; this.update()
    }
    public get y() {
        return this._y
    }
    public set y(value) {
        this._y = value; this.update()
    }
    public get z() {
        return this._z
    }
    public set z(value) {
        this._z = value; this.update()
    }

    private update() {
        this.listeners.forEach(f => f())
    }

    add_listener(f: () => void) {
        this.listeners.add(f)
    }

}


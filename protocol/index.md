
# Libreverse

OUT OF DATE!!

## Transport layer

One of
- TCP (port: 47251)
- UDP (port: 47252)
- websocket (port: 47253, route: /)

## Representation 

### Packets

Types in format `<type used in json>/<type used in binary format>` or `<type used in both>`

#### JSON
#### Binary
TODO

## Packets

### Welcome (sc,cs)
- supported_extensions (string[])
- required_extensions (string[])

### Spawn/Update entity (sc)
- id (i64)
- position (f64,f64,f64)
- rotation (f64,f64,f64)
- scale (f64,f64,f64)
- type: (i64, enum)
- ... extra type-dependent data

### Remove entity (sc)
- id (i64)

### spawn player (cs)
- name (string)

### input (cs)
- type (i64, enum)
- value (f64)

## Entity Types

### `model`
- path (string)

### `light`
- intensity (f64)
- color (string/u32)

### `camera`
- fov (f64)


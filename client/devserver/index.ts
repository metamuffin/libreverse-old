import Express, { static as estatic, json } from "express";
import { join } from "path";
import Webpack from "webpack"
import WebpackDevMiddleware from "webpack-dev-middleware"
import { existsSync, readFile, readFileSync } from "fs";

const app = Express();
app.disable("x-powered-by");

console.log("please dont use this for production");
const webpackConfig = require('../webpack.dev');
const compiler = Webpack(webpackConfig)
const devMiddleware = WebpackDevMiddleware(compiler, {
    publicPath: webpackConfig.output.publicPath
})
app.use("/js", devMiddleware)


app.get("/", (req, res) => {
    res.sendFile(join(__dirname, "../public/index.html"));
});

app.use("/", estatic(join(__dirname, "../public")));

app.use((req, res, next) => {
    res.status(404);
    res.send("This is an error page");
});

const port = parseInt(process.env.PORT ?? "8080")
app.listen(port, process.env.HOST ?? "127.0.0.1", () => {
    console.log(`Server listening on ${process.env.HOST ?? "127.0.0.1"}:${port}`);
})
import { BoxGeometry, Mesh, MeshNormalMaterial, PerspectiveCamera, PointLight, Raycaster, Scene, WebGLRenderer } from 'three';
import { Packet, PacketCS, PacketSC } from '../../common/packet';
import { ClientEntity } from './entity';

import Stats from "three/examples/jsm/libs/stats.module"
import { current_client_time, get_object_owner, set_server_time } from './helper';
import { Animation } from '../../common/animation';
import { ClientAnimation } from './animation';

window.onload = () => {
    init();
}

export let active_camera: PerspectiveCamera;
export const set_active_camera = (c: PerspectiveCamera) => active_camera = c
export let scene: Scene
export let renderer: WebGLRenderer;
let socket: WebSocket
let stats: Stats
let pointer: { x: number, y: number } = { x: 0, y: 0 }
let raycaster: Raycaster

function init() {

    active_camera = new PerspectiveCamera(70, window.innerWidth / window.innerHeight, 0.01, 10);
    active_camera.position.z = 10;

    scene = new Scene();

    raycaster = new Raycaster()

    renderer = new WebGLRenderer({ antialias: true });
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.setAnimationLoop(animation);
    document.body.appendChild(renderer.domElement);

    socket = new WebSocket(`${window.location.protocol.endsWith("s:") ? "wss" : "ws"}://${window.location.hostname}:8081/`)
    socket.onopen = () => console.log("op")
    socket.onclose = () => {
        console.log("close")
        const retry = () => {
            console.log("retry")
            let tws = new WebSocket(`${window.location.protocol.endsWith("s:") ? "wss" : "ws"}://${window.location.hostname}:8081/`)
            tws.onopen = () => window.location.reload()
            tws.onclose = () => setTimeout(retry, 500)
        }
        retry()
    }

    socket.onmessage = ev => {
        const packet: PacketSC = JSON.parse(ev.data)
        console.log(packet);

        if (packet.kind == "welcome") {
            set_server_time(packet.time)
        }
        if (packet.kind == "animate_entity") {
            const a = new ClientAnimation(packet)
            Animation.queue_animation(a)
        }

        if (packet.kind == "update_entity") {
            let e = ClientEntity.by_id.get(packet.id)
            if (!e) {
                e = ClientEntity.spawn(packet)
                ClientEntity.by_id.set(packet.id, e)
                e.load(packet)
            } else {
                e.apply_update(packet)
            }
        }
    }

    document.addEventListener("resize", () => {
        active_camera.aspect = window.innerWidth / window.innerHeight;
        active_camera.updateProjectionMatrix();
        renderer.setSize(window.innerWidth, window.innerHeight);
    })

    document.body.addEventListener("mousemove", ev => {
        pointer.x = (ev.clientX / window.innerWidth) * 2 - 1;
        pointer.y = - (ev.clientY / window.innerHeight) * 2 + 1;
    })

    window.addEventListener("resize", ev => {
        active_camera.aspect = window.innerWidth / window.innerHeight;
        active_camera.updateProjectionMatrix();

        renderer.setSize(window.innerWidth, window.innerHeight);
    })

    document.addEventListener("mousedown", ev => {
        raycaster.setFromCamera(pointer, active_camera)
        const intersections = raycaster.intersectObjects([scene], true);
        for (const i of intersections) {
            let owner = get_object_owner(i.object)
            if (!owner?.clickable) continue

            send_packet({ kind: "input", action: "walk", target: { x: i.point.x, y: i.point.y, z: i.point.z } })
            break
        }
    })

    stats = Stats();
    document.body.appendChild(stats.dom);

}

export function send_packet(p: PacketCS) {
    const j = JSON.stringify(p)
    socket.send(j)
}

function animation(time: number) {
    Animation.update(current_client_time())
    stats.update()
    renderer.render(scene, active_camera);
}

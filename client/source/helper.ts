import { Group, Object3D } from "three";
import { ClientEntity } from "./entity";


export function traverse_group(g: Object3D, f: (o: Object3D) => void) {
    if (g instanceof Group)
        for (const c of g.children)
            traverse_group(c, f)
    f(g)
}

export function get_object_owner(g: Object3D): undefined | ClientEntity {
    const o = ClientEntity.by_object_uuid.get(g.uuid)
    if (o) return o
    else if (g.parent) return get_object_owner(g.parent)
    else throw new Error("object has no owner");
}

let time_offset = 0
export function set_server_time(t: number) {
    time_offset = t - Date.now()
}
export function current_client_time(): number {
    return Date.now() + time_offset
}

// traverse_group(this.object, o => {
//     if (o instanceof Mesh) {
//         if (o.geometry instanceof BufferGeometry) {
//             o.geometry.computeBoundingBox()
//             console.log(o);
//         }
//     }
// })
